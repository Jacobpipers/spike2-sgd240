# Spike Report

## SPIKE 2 - C++

### Introduction

We wish to use Unreal Engine’s C++ to code throughout the semester,
but we do not have a solid understanding. We have experience in a number of programming languages,
but little understanding of C++. Unreal Engine’s C++ can be easier to learn than standalone C++,
but we should still understand the language before continuing.

### Goals

1.	A simple numeric guessing game on the Command Line/Console, written in C++
    1.	A random number between 0-99 (inclusive) is selected by the program to be guessed by the user
    2.	The user is prompted to enter a number until they do so 
        i.	Ignoring non-numeric input, and prompting the user to try again
    3.	Once a number has been given, it is checked for correctness
        i.	If correct, the game ends, and the user is told how many guesses they made
    4.	If not correct, the user is told whether the number they are seeking is smaller or larger than the input number



### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* https://www.sololearn.com/Play/CPlusPlus/#_=_
* https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
* http://www.cplusplus.com/reference/istream/istream/peek/
* http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
* Visual Studio


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

These steps assumes that you have done the SoloLearn C++ tutorials from basic concepts to the Classes and Objects.


1. Create a c++ project ussing visual studio.

2. Print a message to the user informing them about the game and how to play.
    1. print out to the console ussing cout.
    2. Set a random number as a valid guess between 0 and 99.  [Random Refrence](http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution), [Random Refrence 2](https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c)
3. Ask the user for input.
4. Validate the input to see it is a valid option, print a message to inform user if not.
    1. Check to see if the input is between 0 and 99
    2. make sure it does not contain any special characters or non-numeric values - [Code Sample](http://www.learncpp.com/cpp-tutorial/5-10-stdcin-extraction-and-dealing-with-invalid-text-input/)
    3. Make sure it is a intergear value by casting to int value.
    4. Print a corrosponding error message to the user if input is not valid.
5. Print a message telling the user if they got the right answer.
    1. Check to see if user input is the same as the valid guess value
6. If they got the wrong answer ask them again telling them if higher or lower. and Increment the amount of turns.
7. Once the user gets the right answer or they have no more guesses ask the user if they want to play again.


### What we found out

We found out how to create a project in visual studio and how to create a solution for projects.
We found out how to use basic tools in Visual Studio such as auto complete to increase productivity.
We Found out the syntax of c++ and how to use it to make a console program.



### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.