#include "NumberGuess.h"

FNumberGuess::FNumberGuess() { Reset(); }

int32 FNumberGuess::GetCurrentTry() const { return MyCurrentTry; }

FGuessRange FNumberGuess::GetRange()
{
	return Range;
}

bool FNumberGuess::IsGameWon() const { return bGameIsWon; }

int32 FNumberGuess::GetMaxTries() const
{
	return 20;
}

void FNumberGuess::Reset()
{
	// random
	MyHiddenNumber = RNG::RandomRange(Range.min, Range.max); // Set Random Number 

	MyCurrentTry = 1;
	bGameIsWon = false;
	return;
}

EGuessStatus FNumberGuess::CheckGuessValidity(int32 Guess) const
{
	// if the Guess isn't an isogram
	if (!IsNumber(Guess))
	{
		return EGuessStatus::Not_Number;
	}
	else if (Guess	> Range.max || Guess < Range.min)
	{
		return EGuessStatus::Out_Range;
	} 
	else if (std::cin.peek() == '.')
	{
		std::cin.clear();
		std::cin.ignore(256, '\n');
		return EGuessStatus::Not_Int;
	}
	else if (!IsMixed())
	{
		return EGuessStatus::Mix_Input;
	}
	else
	{
		return EGuessStatus::OK;
	}
}

// Recieves a valid guess, incraments turn, and returns count
FString FNumberGuess::SubmitValidGuess(int32 Guess)
{
	MyCurrentTry++;

	// see if game is won
	bGameIsWon = Guess == MyHiddenNumber;

	if (bGameIsWon)
	{
		return "The Number is Correct \n";
	} 
	else if (Guess < MyHiddenNumber)
	{
		return "The Number is higher \n";
	} 
	else
	{
		return "Try a lower number \n";
	}
	
	return "";
}

bool FNumberGuess::IsMixed() const
{
	//char c;
	char got;
	do
	{
		//c = std::cin.peek();
		got = std::cin.get();
		if (!isdigit(got) && got != '\n')
		{
			return false;
		}
	} while (got != '\n');

	return true;
}

bool FNumberGuess::IsNumber(int32 input) const
{
	if (std::cin.fail()) {
		std::cin.clear();
		std::cin.ignore(256, '\n');
		return false;
	}
	else
	{
		return true;
	}
	return true; // just in case
}

