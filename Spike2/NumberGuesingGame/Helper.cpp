#include "Helper.h"
#include <random>


int RNG::RandomRange(int Min, int Max)
{
	int32 number;
	std::random_device rd; //Will be used to obtain a seed for the random number engine
	std::mt19937 rng(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<int> uni(Min, Max);
	number = uni(rng);
	return number;
}
