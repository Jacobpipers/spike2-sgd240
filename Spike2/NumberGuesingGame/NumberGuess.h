#pragma once
#include <string>
#include <map>
#include <iostream>
#include <cctype>

#include "Helper.h"

using FString = std::string;
using int32 = int;

struct FGuessRange
{
	int32 min = 0;
	int32 max = 99;
};

enum class EGuessStatus
{
	Invalid_Status,
	OK,
	Not_Number,
	Out_Range,
	Mix_Input,
	Not_Int
};



class FNumberGuess
{
public:
	FNumberGuess(); // constructor

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	FGuessRange GetRange();
	bool IsGameWon() const;
	EGuessStatus CheckGuessValidity(int32) const;
	void Reset();
	FString SubmitValidGuess(int32);

private:
	// see constructer for initialisation
	int32 MyCurrentTry;
	int32 MyWordLength;
	int32 MyHiddenNumber;
	FGuessRange Range;
	bool bGameIsWon;
	bool IsNumber(int32) const;
	bool IsMixed() const;
};
