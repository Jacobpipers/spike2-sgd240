#pragma once
#include <iostream>
#include <string>
#include "NumberGuess.h"

using FText = std::string;
using int32 = int;

void PrintIntro();
void PlayGame();
bool AskToPlayAgain();
void PrintGameSummary();
int32 GetValidGuess();

FNumberGuess NGGame; // Instantate a new game