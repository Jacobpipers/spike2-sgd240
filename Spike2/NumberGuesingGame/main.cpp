/* This is the console excubatable, that makes use of the NumberGuessingGame class
This acts as the view in a MVC pattern, and is responisble for all
user interaction. For game logic
*/
#include "main.h"


// The Entry point for our application
int main()
{
	do
	{
		PrintIntro();
		PlayGame();
	} while (AskToPlayAgain());

	system("pause");

	return 0; // Exit the application.
}

// Inform the user About the game and what is required.
void PrintIntro()
{
	std::cout << "Welcome to the Number Guessing game, a fun game.\n";
	std::cout << std::endl;
	
	std::cout << "Can you guess the number between " << NGGame.GetRange().min <<" and " << NGGame.GetRange().max;
	std::cout << " I'm thinking of?\n";
	std::cout << std::endl;
	return;
}

// Reset Game and Enter into game loop.
void PlayGame()
{
	// Reset Game Values.
	NGGame.Reset();
	
	int32 MaxTries = NGGame.GetMaxTries();
	std::cout << "Number of Tries: " << MaxTries << std::endl;

	// Loop asking for guesses while game
	// is NOT won and there are still tries remaining
	while (!NGGame.IsGameWon() && NGGame.GetCurrentTry() <= MaxTries) // TODO change from FOR to WHILE loop
	{
		int32 Guess = GetValidGuess(); // TODO make loop valid

		// Submit valid guess to the game
		FString GuessStatus = NGGame.SubmitValidGuess(Guess);

		// Tell user if the Guess is correct.
		std::cout << "" << GuessStatus << std::endl;
	}

	// Summarise game
	PrintGameSummary();

	return;
}

// loop continuly until user gives a valid guess 
int32 GetValidGuess() // 
{
	EGuessStatus Status = EGuessStatus::Invalid_Status;

	int32 Guess = 0;

	do
	{
		// Get a guess from the player
		int32 CurrentTry = NGGame.GetCurrentTry();
		std::cout << "Try: " << CurrentTry << " of " << NGGame.GetMaxTries();
		std::cout << ". Enter your guess: ";


		// ask user for there guess
		std::cin >> Guess;

		Status = NGGame.CheckGuessValidity(Guess);
		
		switch (Status)
		{
		case EGuessStatus::OK:
			return Guess;
			break;
		case EGuessStatus::Not_Number:
			std::cout << "Please enter a Valid number. \n\n";
			break;
		case EGuessStatus::Out_Range:
			std::cout << "Please enter a number between 0 and 99 .\n\n";
			break;
		case EGuessStatus::Not_Int:
			std::cout << "Please enter whole numbers. \n\n";
			break;
		case EGuessStatus::Mix_Input:
			std::cout << "Please enter Numbers only! No Special characters, WhiteSpace and no Characters. \n\n";
		default:
			// assume the guess is valid
			break;
		}
	} while (Status != EGuessStatus::OK); // Keep Looping till get no errors

	return Guess;
}

void PrintGameSummary()
{
	if (NGGame.IsGameWon())
	{
		std::cout << "WELL DONE - YOU WIN! \n";
	}
	else
	{
		std::cout << "Better luck next time \n";
	}
	return;
}


bool AskToPlayAgain()
{
	std::cout << "Do you want to play again (y/n)? ";
	FText Response = "";
	
	std::getline(std::cin, Response);

	return (Response[0] == 'y') || (Response[0] == 'Y');
}
